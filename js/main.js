/*
 * Main Module
*/
(function () {

    //
    // Wait for Cordova to load
    //
    try {
        localStorage.setItem("test", 1);
        localStorage.removeItem("test");
        //we have localstorage
        onDeviceReady();
    } catch(e) {
        //we don't have localstorage 
        document.addEventListener("deviceready", onDeviceReady, false);
    }

    //
    // Cordova is ready
    //
    function onDeviceReady() {
        hagakure.appControl.init("panel", "list", "textInput", "textAddButton");
        hagakure.appControl.raiseEvent("loadItems");

        hagakure.appControl.subscribeEvent("addItem", function () {
            hagakure.appControl.raiseEvent("saveItems");
        });

        hagakure.appControl.subscribeEvent("removeItem", function () {
            hagakure.appControl.raiseEvent("saveItems");
        });
    }

})();