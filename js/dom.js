/*
 * Module for creating items in DOM
 *
 * createItem(text for item, properties):
 * properties: {
 *     tag: 'div',              //Its tag that will be using for element creation
 *     parentId: 'list',        //Id of parent container
 *     event: {
 *         type: 'onClick',     //Type of event
 *         call: function(){},  //Callback that will be calling on event
 *         target: 'a'          //Tag of children element, which will be evented
 *     }
 * }
*/
(function(global){
    var DOMC = function(){};

    //Set default template for new element
    //==============================================================
    DOMC.prototype.template = '<div class="item-content">' +
                                    '{#text}' +
                                 '</div>' +
                                 '<a>×</a>';

    //Function for creating new item
    //==============================================================
    DOMC.prototype.createItem = function(text,properties){
        var re = /{[^{}]+}/g;

        //Create new DOM element
        var ne = document.createElement(properties.tag);
        var parent = document.getElementById(properties.parentId);

        //Insert text
        ne.innerHTML = this.template.replace(re,text);

        //Set default properties
        ne.className = 'item';

        //Attach event if it have
        if (properties.event != undefined){
            switch(typeof properties.event.target){
                case 'string':
                    var eef = ne.getElementsByTagName(properties.event.target)[0];
                    eef.addEventListener(properties.event.type,properties.event.call);
                    break;
                default:
                    ne.addEventListener(properties.event.type,properties.event.call);
                    break;
            }
            delete properties.event;
        }

        //Delete system options
        delete properties.tag;
        delete properties.parentId;

        //Set attributes for new element
        for(var i in properties){
            ne.setAttribute(i,properties[i]);
        }

        //Add to the parent element
        parent.appendChild(ne);
    }

    //Delete element
    //==============================================================
    DOMC.prototype.deleteItem = function(el){
        switch(typeof el){
            case 'object':
                el.parentNode.removeChild(el);
                break;
            case 'string':
                var del = document.getElementById(el);
                this.deleteItem(del);
                break;
            default:
                break;
        }
    }

    //Get elements by attribute
    //==============================================================
    DOMC.prototype.getElements = function(selector){
        var matchingElements = [];
        var allElements = document.getElementsByTagName('*');

        //Go by all DOM tree
        for (var i = 0,l=allElements.length; i < l; i++)
        {
            //If we found element with needed attribute
            if (allElements[i].getAttribute(selector))
            {
                // Element exists with attribute. Add to array.
                matchingElements.push(allElements[i]);
            }
        }
        return matchingElements;
    };

    //Save data to storage
    DOMC.prototype.setData = function(data){
        var dt = {
            date: new Date().getDate(),
            data: data
        };

        localStorage.setItem('hagakure',JSON.stringify(dt));
    };

    //Get data from storage
    DOMC.prototype.getData = function(){
        var dt = JSON.parse(localStorage.getItem('hagakure'));

        return dt && dt.data ? dt.data : [];
        //return (dt != null && dt.data != null && dt.date != null && dt.date == new Date().getDate()) ? dt.data : [];
    };

    //Initialize module
    //==============================================================
    global.DOMC = new DOMC();

})(window);

if (typeof window.localStorage == 'undefined' || typeof window.sessionStorage == 'undefined') (function () {


var Storage = function (type) {
  function createCookie(name, value, days) {
    var date, expires;


    if (days) {
      date = new Date();
      date.setTime(date.getTime()+(days*24*60*60*1000));
      expires = "; expires="+date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
  }


  function readCookie(name) {
    var nameEQ = name + "=",
        ca = document.cookie.split(';'),
        i, c;


    for (i=0; i < ca.length; i++) {
      c = ca[i];
      while (c.charAt(0)==' ') {
        c = c.substring(1,c.length);
      }


      if (c.indexOf(nameEQ) == 0) {
        return c.substring(nameEQ.length,c.length);
      }
    }
    return null;
  }
  
  function setData(data) {
    data = JSON.stringify(data);
    if (type == 'session') {
      window.name = data;
    } else {
      createCookie('localStorage', data, 365);
    }
  }
  
  function clearData() {
    if (type == 'session') {
      window.name = '';
    } else {
      createCookie('localStorage', '', 365);
    }
  }
  
  function getData() {
    var data = type == 'session' ? window.name : readCookie('localStorage');
    return data ? JSON.parse(data) : {};
  }




  // initialise if there's already data
  var data = getData();


  return {
    length: 0,
    clear: function () {
      data = {};
      this.length = 0;
      clearData();
    },
    getItem: function (key) {
      return data[key] === undefined ? null : data[key];
    },
    key: function (i) {
      // not perfect, but works
      var ctr = 0;
      for (var k in data) {
        if (ctr == i) return k;
        else ctr++;
      }
      return null;
    },
    removeItem: function (key) {
      delete data[key];
      this.length--;
      setData(data);
    },
    setItem: function (key, value) {
      data[key] = value+''; // forces the value to a string
      this.length++;
      setData(data);
    }
  };
};


if (typeof window.localStorage == 'undefined') window.localStorage = new Storage('local');
if (typeof window.sessionStorage == 'undefined') window.sessionStorage = new Storage('session');


})();


